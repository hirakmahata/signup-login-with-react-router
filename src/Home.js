import React from "react"
import axios from "axios"


export default class Home extends React.Component {
    constructor() {
        super()
        this.state = {
            notFollowers: [],
            followerTweets: [],
            followClicked: false,
            name: "",
            tweet: "",
            tweetResult: "",
            tweetErr: "",
            followerName:"",
            followerTweet: [],
            followerFullName: ""
        }
    }

    componentDidMount(){
        axios({
            method: 'post',
                url: 'https://signup-login-backend-hirak.herokuapp.com/home',
                data: {
                    user_id: localStorage.getItem("user_id")
                }
        })
        .then(response=>{
            this.setState({
                notFollowers: response.data.follower_data,
                followerTweets: response.data.follower_tweets
            })
            console.log(response.data.follower_data)
        });
    }


    handleChange = (event) => {
        this.setState({
            tweet: event.target.value
        })
    }

    handleFollow = (event) => {
        this.setState({
            followClicked:true
        })
        
        axios({
            method: 'post',
            url: 'https://signup-login-backend-hirak.herokuapp.com/follower',
            data: {
                follower_id : event.target.id,
                followerName: event.target.name,
                user_id: localStorage.getItem("user_id")
            }
        })
        .then(response => {
            this.setState({
                followerTweet: response.data.tweet,
                followerFullName: response.data.name
            })
        })
    }

    handleTweet = () => {
        axios({
            method: 'post',
            url: 'https://signup-login-backend-hirak.herokuapp.com/tweet',
            data: {
                email: localStorage.getItem("email"),
                tweet: this.state.tweet
            }
        })
            .then(response => {
                if (response.data.message === "tweet can not be empty") {
                    this.setState({
                        tweetErr: response.data.message
                    })
                } else {
                    this.setState({
                        tweetResult: response.data.tweet,
                        name: response.data.name
                    })
                }
            })
    }

    render = () => (
        <div className="container-fluid border row" style={{ height: "100vh", width: "100vw" }}>
            <div className="col-md-3 column " >
                <div className="d-flex justify-content-start">
                    <i className="fab fa-twitter fa-2x p-3" style={{ color: "blue" }}></i>
                </div>


                <div className="d-flex justify-content-start">
                    <i className="fas fa-home fa-lg p-3" style={{ color: "blue" }}></i>
                    <span><h4 className="p-2">Home</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <h3 className="p-3">#</h3>
                    <span><h4 className="p-3">Explore</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <i className="far fa-bell fa-lg p-3"></i>
                    <span><h4 className="p-2">Notifications</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <i className="far fa-envelope fa-lg p-3"></i>
                    <span><h4 className="p-2">Messages</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <i className="far fa-bookmark fa-lg p-3"></i>
                    <span><h4 className="p-2">Bookmarks</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <i className="far fa-list-alt fa-lg p-3"></i>
                    <span><h4 className="p-2">Lists</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <i className="far fa-user fa-lg p-3"></i>
                    <span><h4 className="p-2">Profile</h4></span>
                </div>
                <div className="d-flex justify-content-start">
                    <i className="fas fa-angle-double-right fa-lg p-3"></i>
                    <span><h4 className="p-2">More</h4></span>
                </div>
                <div className="d-flex justify-content-center">
                    <button className="btn btn-primary rounded-pill form-control ">Tweet</button>
                </div>
            </div>
            <div className="col-md-6 border">
                <div className="container-fluid d-flex  justify-content-between ">
                    <h4 className="p-3">Home</h4>
                    <i className="fas fa-hand-sparkles p-2 mt-3"></i>
                </div>
                <hr></hr>
                <div className="container-fluid d-flex">
                    <i className="fas fa-user-circle fa-3x"></i>
                    <span><input
                        type="text"
                        className=" tweet mx-1 px-3 "
                        placeholder="What's happening"
                        onChange={this.handleChange}
                        style={{ border: "none", fontSize: "20px" }}
                    ></input></span>
                </div>
                <div className="d-flex" style={{ paddingLeft: "90px" }}>
                    <i className="fas fa-globe-americas pl-3 " style={{ color: "blue" }}></i>
                    <p className=" px-2 " style={{ color: "blue" }}>Everyone can reply</p>
                </div>
                <hr></hr>
                <div className="d-flex justify-content-between">
                    <div>
                        <i className="far fa-image fa-2x px-1" style={{ color: "blue" }}></i>
                        <i className="far fa-smile fa-2x px-1" style={{ color: "blue" }}></i>
                    </div>

                    <button className="btn btn-primary rounded-pill justify-content-end"
                        onClick={this.handleTweet}
                        disabled={this.state.tweet === "" ? true : false}
                    >Tweet</button>
                </div>
                <hr></hr>


                {this.state.tweetResult !== "" ? <div>
                    <div className="container-fluid d-flex">
                        <i className="fas fa-user-circle fa-3x "></i>
                        <p className="px-3 align-self-center"><strong>{this.state.name}</strong> @username</p>

                    </div>
                    <div className="px-5"><p>{this.state.tweetResult}</p></div>
                </div> : ""}


                {this.state.followerTweet.length !== 0 ?  this.state.followerTweet.map(tweet=>{
                    return (<div>
                        <div className="container-fluid d-flex">
                            <i className="fas fa-user-circle fa-3x "></i>
                            <p className="px-3 align-self-center"><strong>{this.state.followerFullName}</strong> @username</p>
    
                        </div>
                        <div className="px-5"><p>{tweet}</p></div>
                    </div>)
                }): ""}

                {this.state.followerTweets.map(details=>{
                   let info= details.tweet.map(tweet=>{
                        return (<div>
                            <div className="container-fluid d-flex">
                                <i className="fas fa-user-circle fa-3x "></i>
                                <p className="px-3 align-self-center"><strong>{details.name}</strong> @username</p>
        
                            </div>
                            <div className="px-5"><p>{tweet}</p></div>
                        </div>)
                    })
                    return info ;
                })}
                

            </div>
            <div className="col-md-3 column ">
                <h4 className="m-4">Who to follow</h4>

                {this.state.notFollowers.map(notFollower=>{
                    return (<div className="d-flex justify-content-between my-3">
                    <div className="d-flex">
                        <i className="fas fa-user-circle fa-2x "></i>
                        <p className="px-3"><strong>{notFollower.name}</strong>@username</p>
                    </div>

                    <button className="btn btn-primary rounded-pill justify-content-end"
                    onClick={this.handleFollow}
                    name={notFollower.name}
                    id ={notFollower.id}
                    // disabled={this.state.followClicked}
                    >{ !this.state.followClicked ? "follow": "following" }</button>
                </div>)
                
                })}
        
            </div>
        </div>
    )
}