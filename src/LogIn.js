import React from "react"
import Validator from "./LogInValidation"
import { Link } from "react-router-dom"
import axios from "axios"
import Loader from "./Loader"

export default class Login extends React.Component {
    constructor() {
        super()
        this.state = {
            email: "",
            password: "",
            errors: {
                email: "",
                password: ""
            },
            loginErr: "",
            loading: false

        }
    }

    handleChange = async (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        const result = await Validator({ [name]: value })

        this.setState({ [name]: value }, () => {
            if (result !== "success") {
                this.setState((prevState) => ({
                    errors: {
                        ...prevState.errors,
                        [name]: result
                    }
                }));
            } else {
                this.setState(prevState => ({
                    errors: {
                        ...prevState.errors,
                        [name]: ""
                    }
                }));
            }
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        this.setState({
            loading: true
        })


        if (
            this.state.errors.email === ""
            && this.state.errors.password === ""
        ) {
            axios({
                method: 'post',
                url: 'https://signup-login-backend-hirak.herokuapp.com/logIn',
                data: {
                    email: this.state.email,
                    password: this.state.password
                }
            })
                .then(response => {
                    localStorage.setItem("user_id", response.data.user.id)

                    this.setState({
                        loading:false,
                        loginErr: response.data.message
                    }, () => {
                        if (!response.data.message
                            && response.data.user.email !== ""
                            && response.data.user.password !== "") {

                            this.props.history.push("/home");
                        }

                    })
                })
                .catch(err => {
                    console.error(err);
                })
        }
        
    }

    render = () => (
        <div className="wrapper container-fluid d-flex justify-content-center " style={{ height: "100vh" }}>
            <div className="col-md-4  m-1 my-5 border border-dark  bg-white">
                <nav className="container-fluid navbar-dark bg-dark">
                    <h2 className="text-center text-white p-2">WELCOME TO OUR WEBSITE</h2>
                </nav>
                <form onSubmit={this.handleSubmit} className="p-1 px-3">

                    <div>
                        <h2 className="text-center">LOGIN</h2>
                        <p className="text-center">Login with your email and password</p>
                        </div>
                    <strong className="mt-3">Email:</strong>
                    <div className="inputWithIcon">
                        <input
                            className="rounded form-control"
                            type="text"
                            name="email"
                            placeholder="Enter your email"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                        <i class="fas fa-envelope" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-4">{this.state.errors.email}</div>
                    <strong className="mt-3">Password:</strong>
                    <div className="inputWithIcon">
                        <input
                            className=" form-control"
                            type="password"
                            name="password"
                            placeholder="Enter your password"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                        <i class="fas fa-lock" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-4" >{this.state.errors.password}</div>
                    {this.state.loading ? <Loader /> : "" }
                    <button
                    disabled ={this.state.loading}
                        className="my-2 mt-5 btn btn-outline-primary rounded form-control "
                        style={{ outline: "none" }}
                    >Log in  </button> 

                    {this.state.loginErr === "email does not exist. please sign up" ?
                        <div className="text-center text-danger mt-1">{this.state.loginErr}</div> :
                        this.state.loginErr === "incorrect password. please enter correct password" ?
                            <div className="text-center text-danger mt-1">{this.state.loginErr}</div> :
                            this.state.loginErr === "email or password should not be empty" ?
                                <div className="text-center text-danger mt-1">{this.state.loginErr}</div> : null}

                    <div className="text-center mt-1">
                        <span>Don't have any account ?
                            <Link to="/signup" style={{textDecoration:"none"}}> Sign Up</Link>
                        </span>
                    </div>
                </form>

            </div>
        </div>
    )
}