import Joi from "joi";

const validator = (obj) => {
  return new Promise((resolve, reject) => {
    const schema = Joi.object({
      firstName: Joi.string().min(4),
      lastName:Joi.string(),
      email: Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net", "tech", "io"] }
      }),
      password: Joi.string().min(5),
      repeatPassword: Joi.ref("password")
    });

    const result = schema.validate(obj);

    if (result.error) {
      return resolve(result.error.details[0].message);
    }
    return resolve("success");
  });
};

export default validator;