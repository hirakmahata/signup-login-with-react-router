
import React from "react"
import SignUp from "./SignUp"
import LogIn from "./LogIn"
import Home from "./Home"
import OTP from "./Otp"
import {BrowserRouter as Router, Switch, Route } from "react-router-dom"

class App extends React.Component {

  render = () => (
    <div>
       <Router>
         <Switch>
           <Route path="/" exact component={LogIn} />
           <Route path="/login" component={LogIn} />
           <Route path="/signup" component={SignUp} />
           <Route path="/home" component={Home} />
           <Route path="/otp" component={OTP} />
         </Switch>
       </Router>
       {/* <Home /> */}
    </div>
  )
}

export default App;
