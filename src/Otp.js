import React from "react"
import axios from "axios"
import Loader from "./Loader"


export default class Otp extends React.Component {
   constructor(){
       super()
       this.state = {
            OTP: "",
            OTPErr: "",
            loading:false
       }
   }

   handleChange = (event)=>{
       this.setState({
           OTP : event.target.value
       })
   }

   handleSubmit = (event)=>{
    event.preventDefault();
    this.setState({
        loading:true
    })
        axios({
            method: 'post',
            url: 'https://signup-login-backend-hirak.herokuapp.com/otp',
            data: {
                email: localStorage.getItem("email"),
                OTP : this.state.OTP
            }
        })
        .then(response=>{
            this.setState({
                loading: false,
                OTPErr : response.data.message
            },()=>{
                if(response.data.message === "email verified successfully"){
                    this.props.history.push("./login")
                }
            })
        })

   }

    render = () => (
        <div className="wrapper container-fluid d-flex justify-content-center " style={{ height: "100vh" }}>
            <div className="col-md-4  m-1 my-5 border border-dark  bg-white">
                <nav className="container-fluid navbar-dark bg-dark">
                    <h2 className="text-center text-white p-5">OTP Verification</h2>
                </nav>

                <form className="p-1 px-3" onSubmit={this.handleSubmit}>
                    <h4 className="text-center p-3">Enter your OTP</h4>
                    <div className=" px-5 m-3">
                        <input
                            className="rounded form-control text-center"
                            type="text"
                            name="OTP"
                            placeholder="Enter your OTP"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                    </div>
                    <div className="pt-3 ">
                    <p className="text-center">OTP has been sent to your registered email id. please check email and enter OTP</p>
                    </div>
                    {this.state.loading ? <Loader /> : "" }
                    
                    <div className="px-3">
                        <button
                            className="my-2 mt-5 btn btn-outline-primary rounded form-control "
                            style={{ outline: "none" }}
                        >Submit OTP</button>
                    </div>
                    {this.state.OTPErr==="please enter your OTP"? 
                    <div className="text-center text-danger mt-1">{this.state.OTPErr}</div> :
                    this.state.OTPErr==="OTP is wrong. please enter the correct OTP"?
                    <div className="text-center text-danger mt-1">{this.state.OTPErr}</div> : null
                    }
                </form>


            </div>
        </div>
    )
}