import './App.css';
import React from "react"
import Validator from "./SignUpValidation"
import axios from "axios"
import { Link } from "react-router-dom"
import Loader from "./Loader" 

export default class App1 extends React.Component {
    constructor() {
        super()
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            repeatPassword: '',
            errors: {
                firstName: "",
                lastName: "",
                email: "",
                password: "",
                repeatPassword: ""
            },
            signUpErr: "",
            loading: false
        }
    }
    handleChange = async (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        const result = await Validator({ [name]: value });

        if (name === "repeatPassword") {
            this.setState({
                [name]: value
            }, () => {
                if (this.state.password === value) {
                    this.setState((prevState) => ({
                        errors: {
                            ...prevState.errors,
                            [name]: ""
                        }
                    }));
                } else {
                    this.setState((prevState) => ({
                        errors: {
                            ...prevState.errors,
                            [name]: "password didn't match"
                        }
                    }));
                }
            })

        } else {
            this.setState({ [name]: value }, () => {
                if (result !== "success") {
                    this.setState((prevState) => ({
                        errors: {
                            ...prevState.errors,
                            [name]: result
                        }
                    }));
                } else {
                    this.setState(prevState => ({
                        errors: {
                            ...prevState.errors,
                            [name]: ""
                        }
                    }));
                }
            });
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            loading:true
        })
        if (this.state.errors.firstName === ""
            && this.state.errors.lastName === ""
            && this.state.errors.email === ""
            && this.state.errors.password === ""
            && this.state.errors.repeatPassword === ""
        ) {
            axios({
                method: 'post',
                url: 'https://signup-login-backend-hirak.herokuapp.com/signUp',
                data: {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    email: this.state.email,
                    password: this.state.password
                }
            })
                .then(response => {
                    this.setState({
                        loading: false,
                        signUpErr: response.data.message
                    }, () => {
                        if (this.state.signUpErr === "registered successfully") {
                            localStorage.setItem("email", this.state.email);
                            this.props.history.push("/otp");
                        }
                    })
                })
                .catch(err => {
                    console.error(err)
                })

        }

    }



    render = () => (
        <div className=" wrapper container-fluid d-flex justify-content-center" style={{ height: "100vh" }}>
            <div className="col-md-4 px-4 m-1 border border-dark form-group  bg-white">
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <h1 className="text-center mt-1 ">SIGNUP</h1>
                    </div>
                    <div><p className="text-center">please fill the form to create an Account</p></div>

                    <strong>First Name:</strong>
                    <div className=" form-group inputWithIcon">
                        <input className="form-control"
                            name="firstName"
                            type="text"
                            placeholder="enter your firstName"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                        <i class="fas fa-user" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-2">{this.state.errors.firstName}</div>

                    <strong>Last Name:</strong>
                    <div className="form-group inputWithIcon">

                        <input
                            name="lastName"
                            className=" form-control "
                            type="text"
                            placeholder="enter your LastName"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        />
                        <i class="fas fa-user" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-2">{this.state.errors.lastName}</div>

                    <strong>Email:</strong>
                    <div className="form-group inputWithIcon">

                        <input className=" form-control "
                            name="email"
                            type="text"
                            placeholder="enter your Email"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                        <i class="fas fa-envelope" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-2">{this.state.errors.email}</div>

                    <strong>Password:</strong>
                    <div className="form-group inputWithIcon">

                        <input className=" form-control "
                            type="password"
                            placeholder="enter your password"
                            name="password"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                        <i class="fas fa-lock" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-2">{this.state.errors.password}</div>

                    <strong> Repeat Password:</strong>
                    <div className="form-group inputWithIcon">

                        <input className="form-control "
                            type="password"
                            placeholder="re-enter your password"
                            name="repeatPassword"
                            onChange={this.handleChange}
                            style={{ boxShadow: "0px 2px rgb(0, 1, 5)" }}
                        ></input>
                        <i class="fas fa-lock" aria-hidden="true"></i>
                    </div>
                    <div className="text-danger mb-3 ">{this.state.errors.repeatPassword}</div>
                    {this.state.loading ? <Loader /> : "" }
                    <div >
                        <button className="btn btn btn-outline-primary rounded mt-1 form-control"
                        disabled={this.state.loading}                            
                            style={{ outline: "none" }}
                        >Create Account</button>
                        {this.state.signUpErr === "email already exists"
                            && this.state.signUpErr !== "" ?
                            <div className="text-danger text-center mt-1" >{this.state.signUpErr}</div> :
                            this.state.signUpErr === "no field should be empty" ?
                                <div className="text-danger text-center mt-1" >{this.state.signUpErr}</div> : null
                        }
                        <div  className="text-center">
                            <span>Already have Account ?  
                                <Link to="/login" style={{textDecoration:"none"}}> Log In</Link>
                            </span>
                        </div>
                        
                        
                    </div>
                </form>

            </div>

        </div>
    )
}